fun main() {
    val x = Fraction(16.0, 10.0)
    println(x)
    val y = Fraction(10.0, 18.0)
    println(y)
    println(x.plus(y))
    println(x.multiply(y))
    println(x.minus(y))
    println(x.division(y))
    println(x.simplify())
    println(y.simplify())


}


class Fraction(val nominator: Double, val denominator: Double) {
    override fun toString(): String {
        return "$nominator $denominator"
    }

    override fun equals(other: Any?): Boolean {
        if (other is Fraction) {
            return (nominator * other.denominator == other.nominator * denominator)
        }
        return false
    }

    fun plus(other: Fraction): Fraction {
        val newDenominator = denominator * other.denominator
        val newNominator1 = newDenominator / denominator * nominator
        val newNominator2 = newDenominator / other.denominator * other.nominator
        return Fraction(newNominator1 + newNominator2, newDenominator)

    }

    fun multiply(other: Fraction): Fraction {
        val NewNominator = nominator * other.nominator
        val NewDenominator = denominator * other.denominator
        return Fraction(NewNominator, NewDenominator)

    }


    fun minus(other: Fraction): Fraction {
        return plus(Fraction(-1 * other.nominator, other.denominator))

    }

    fun division(other: Fraction): Fraction {
        return multiply(Fraction(other.denominator, other.nominator))
    }

    fun simplify(): Fraction {
        var gcd: Int = GCD()
        val NewNominator = nominator /gcd
        val NewDenominator = denominator/gcd
        return Fraction(NewNominator, NewDenominator)


    }
    fun GCD(): Int {
        var gcd: Int = 1
        var i: Int = 1
        while (i <= nominator && i <= denominator) {
            if (nominator % i == 0.0 && denominator % i == 0.0)
                gcd = i
            ++i
        }
            return  gcd
    }
}